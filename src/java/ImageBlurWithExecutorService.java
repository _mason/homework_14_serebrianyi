package java;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/*
* Размытие изображения
* */
public class ImageBlurWithExecutorService {

    private static int imgSize;
    //Число пикселей в маске (13 - значит справа и слева по 6)
    private static int mBlurWidth = 13;

    public static void main(String[] args) {
        try {
            System.out.println("Loading image...");

            BufferedImage lImg = ImageIO.read(new File("cat.jpg"));
            System.out.println("Image loaded.");

            imgSize = lImg.getWidth();

            System.out.println("Processing image...");
            BufferedImage rImg = process(lImg);
            System.out.println("Image processed");

            System.out.println("Saving image...");
            ImageIO.write(rImg, "jpg", new File("out.jpg"));
            System.out.println("Image saved");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static BufferedImage process(BufferedImage lImg) throws ExecutionException, InterruptedException {
        //представляем изображение, как массив пикселей ()
        int[] rgb = imgToRgb(lImg);

        long start = System.currentTimeMillis();
        //задаем число потоков и задач
        int[] transformed = blurParallel(rgb, 2, 100);
        long end = System.currentTimeMillis();

        System.out.println((end - start) + "ms");

        //переводим массив пикселей в изображение
        return rgbToImg(transformed);
    }


    private static int[] blurParallel(int[] rgb, int threadsCount, int tasksCount) throws ExecutionException, InterruptedException {
        //результирующий массив
        int[] res = new int[rgb.length];
        final int partSize = rgb.length / tasksCount;
        /*
        * здесь создаем ExecutorService, добавляем в него задачи, которые будут заполнять результирующий массив
        * т.е каждая задача берет на себя расчет определенного количества пикселей(partSize) результирующего массива
        * расчет значений пикселей выполнять в методе computeDirectly
        * ждем пока все задачи отработают и закрываем ExecutorService
        * */

        List<Callable<Void>> task = new ArrayList<>();
        for (int i = 0; i < tasksCount; i++) {
            int finalI = i;
            task.add(() -> {
                computeDirectly(rgb, finalI * partSize, partSize, res);
                return null;
            });

        }
        ExecutorService eService = Executors.newFixedThreadPool(threadsCount);
        List<Future<Void>> futures = new ArrayList<>();
        for (Future f : futures) {
            f.get();
        }
        eService.shutdown();
        return res;
    }

    static protected void computeDirectly(int[] source, int start, int length, int[] destination) {
        /*
        * расчет пикселей начиная с индекса start до start + length
        * тут реализовать алгоритм расчета среднего значения пикселя, исходя из ширины маски (это mBlurWidth)
        * после расчета очередного пикселя, помещаем его в массив destination
        * */

        for (int i = start; i < start + length; i++) {
            int row = i / imgSize;
            int col = i % imgSize;
            int blureHalf = mBlurWidth / 2;

            int red = 0;
            int blue = 0;
            int green = 0;

            for (int j = row - blureHalf; j < row + blureHalf; j++) {
                if (i < 0 || i >= imgSize) continue;
                for (int k = col - blureHalf; k < col + blureHalf; k++) {
                    if (k < 0 || k >= imgSize) continue;

                    int px = source[j * imgSize + k];
                    int r = ((px & 0x00_FF_00_00) >> 16);
                    int g = ((px & 0x00_00_FF_00) >> 8);
                    int b = px & 0x00_00_00_FF;

                    red += r;
                    blue += b;
                    green += g;
                }
                int devide = mBlurWidth*mBlurWidth;
                int px = 0xFF_00_00_00|((red/devide)>>16)|((green/devide)>>8|(blue/devide));
                destination[i]=px;
            }

        }
    }

    private static int[] imgToRgb(BufferedImage img) {
        int[] res = new int[imgSize * imgSize];
        img.getRGB(0, 0, imgSize, imgSize, res, 0, imgSize);
        return res;
    }

    private static BufferedImage rgbToImg(int[] rgb) {
        BufferedImage res = new BufferedImage(imgSize, imgSize, BufferedImage.TYPE_INT_RGB);
        res.setRGB(0, 0, imgSize, imgSize, rgb, 0, imgSize);
        return res;
    }
}
